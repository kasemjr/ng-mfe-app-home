FROM nginx:1.18-alpine

COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf

COPY /dist/ng-mfe-app-home /usr/share/nginx/html
